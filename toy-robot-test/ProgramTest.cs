using Microsoft.VisualStudio.TestTools.UnitTesting;
using ToyRobot.Enums;
using ToyRobot.Models;
using ToyRobot.Interfaces;
using ToyRobot.Models.Commands;
using System.Text;
using System.IO;

namespace ToyRobotTest;

[TestClass]
public class ProgramTest
{
    ConsoleCommand[] _consoleCommands;
    ICommandParser _consoleCommandParser;
    ITable _table;

    public ProgramTest()
    {
        _consoleCommands = new ConsoleCommand[] {
            new CloseCommand(),
            new RobotPlaceCommand(),
            new RobotMoveCommand(),
            new RobotRotateCommand(Rotate.LEFT),
            new RobotRotateCommand(Rotate.RIGHT),
            new RobotReportCommand()
        };
        _consoleCommandParser = new ConsoleCommandParser(_consoleCommands);
        _table = new Table(5, 5);
    }

    [TestMethod("IConsoleCommandParser_Empty")]
    public void IConsoleCommandParser_Empty()
    {
        IConsoleCommand? command;
        Assert.IsFalse(_consoleCommandParser.TryParse("", out command));
        Assert.IsNull(command);
    }

    [TestMethod("IConsoleCommandParser_BadCommand")]
    public void IConsoleCommandParser_BadCommand()
    {
        IConsoleCommand? command;
        Assert.IsFalse(_consoleCommandParser.TryParse("fdgs dfsgsd fg sdf gsdfg", out command));
        Assert.IsNull(command);
    }

    [TestMethod("IConsoleCommandParser_GoodCommands")]
    public void IConsoleCommandParser_GoodCommands()
    {
        foreach (ConsoleCommand consoleCommand in _consoleCommands)
        {
            IConsoleCommand? command;
            Assert.IsTrue(_consoleCommandParser.TryParse(consoleCommand.CommandAction, out command));
            Assert.IsNotNull(command);
        }
    }

    [TestMethod("RobotCommands_PLACENoArgs")]
    public void RobotCommands_PLACENoArgs()
    {
        //PLACE with no arguments
        Robot robot = new Robot(_table);
        RobotPlaceCommand command = new RobotPlaceCommand();
        command.Execute(robot);
        Assert.IsFalse(robot.IsPlaced);
    }

    [TestMethod("RobotCommands_PLACE2Args")]
    public void RobotCommands_PLACE2Args()
    {
        //PLACE with 2 arguments
        Robot robot = new Robot(_table);
        RobotPlaceCommand command = new RobotPlaceCommand()
        {
            CommandArgs = new string[] { "1", "2" }
        };
        command.Execute(robot);
        Assert.IsFalse(robot.IsPlaced);
    }

    [TestMethod("RobotCommands_PLACE3Args")]
    public void RobotCommands_PLACE3Args()
    {
        //PLACE with 2 arguments
        Robot robot = new Robot(_table);
        RobotPlaceCommand command = new RobotPlaceCommand()
        {
            CommandArgs = new string[] { "1", "2", "WEST" }
        };
        command.Execute(robot);
        Assert.IsTrue(robot.IsPlaced);
        Assert.AreEqual(robot.X, 1);
        Assert.AreEqual(robot.Y, 2);
        Assert.AreEqual(robot.Direction, Direction.WEST);
    }

    [TestMethod("RobotCommands_PLACE3ArgsBadPosition")]
    public void RobotCommands_PLACEWith3ArgsBadPosition()
    {
        //PLACE with 3 arguments but bad direction
        Robot robot = new Robot(_table);
        RobotPlaceCommand command = new RobotPlaceCommand()
        {
            CommandArgs = new string[] { "1", "2", "sdfgsdfgsdg" }
        };
        command.Execute(robot);
        Assert.IsFalse(robot.IsPlaced);
    }

    [TestMethod("RobotCommands_PLACE3ArgsOutOfBounds")]
    public void RobotCommands_PLACE3ArgsOutOfBounds()
    {
        //PLACE with 3 arguments but out of bounds
        Robot robot = new Robot(_table);
        RobotPlaceCommand command = new RobotPlaceCommand()
        {
            CommandArgs = new string[] { "8", "2", "WEST" }
        };
        command.Execute(robot);
        Assert.IsFalse(robot.IsPlaced);
    }

    [TestMethod("RobotCommands_PLACESouthWestBound")]
    public void RobotCommands_PLACESouthWestBound()
    {
        //PLACE south west
        Robot robot = new Robot(_table);
        RobotPlaceCommand command = new RobotPlaceCommand()
        {
            CommandArgs = new string[] { "0", "0", "SOUTH" }
        };
        command.Execute(robot);
        Assert.IsTrue(robot.IsPlaced);
        Assert.AreEqual(robot.X, 0);
        Assert.AreEqual(robot.Y, 0);
        Assert.AreEqual(robot.Direction, Direction.SOUTH);
    }

    [TestMethod("RobotCommands_PLACENorthEastBound")]
    public void RobotCommands_PLACENorthEastBound()
    {
        //PLACE north east
        Robot robot = new Robot(_table);
        RobotPlaceCommand command = new RobotPlaceCommand()
        {
            CommandArgs = new string[] { "5", "5", "WEST" }
        };
        command.Execute(robot);
        Assert.IsTrue(robot.IsPlaced);
        Assert.AreEqual(robot.X, 5);
        Assert.AreEqual(robot.Y, 5);
        Assert.AreEqual(robot.Direction, Direction.WEST);
    }

    [TestMethod("RobotCommands_MOVEEast")]
    public void RobotCommands_MOVEEast()
    {
        //PLACE facing east
        Robot robot = new Robot(_table);
        RobotPlaceCommand command = new RobotPlaceCommand()
        {
            CommandArgs = new string[] { "0", "0", "EAST" }
        };
        command.Execute(robot);

        //MOVE
        RobotMoveCommand command2 = new RobotMoveCommand();
        command2.Execute(robot);

        //Validate
        Assert.AreEqual(robot.X, 1);
        Assert.AreEqual(robot.Y, 0);
        Assert.AreEqual(robot.Direction, Direction.EAST);
    }

    [TestMethod("RobotCommands_MOVENorth")]
    public void RobotCommands_MOVENorth()
    {
        //PLACE facing north
        Robot robot = new Robot(_table);
        RobotPlaceCommand command = new RobotPlaceCommand()
        {
            CommandArgs = new string[] { "0", "0", "NORTH" }
        };
        command.Execute(robot);

        //MOVE
        RobotMoveCommand command2 = new RobotMoveCommand();
        command2.Execute(robot);

        //Validate
        Assert.AreEqual(robot.X, 0);
        Assert.AreEqual(robot.Y, 1);
        Assert.AreEqual(robot.Direction, Direction.NORTH);
    }

    [TestMethod("RobotCommands_MOVEOutOfBounds")]
    public void RobotCommands_MOVEOutOfBounds()
    {
        //PLACE north east
        Robot robot = new Robot(_table);
        RobotPlaceCommand command = new RobotPlaceCommand()
        {
            CommandArgs = new string[] { "5", "5", "NORTH" }
        };
        command.Execute(robot);

        //MOVE
        RobotMoveCommand command2 = new RobotMoveCommand();
        command2.Execute(robot);

        //Validate
        Assert.AreEqual(robot.X, 5);
        Assert.AreEqual(robot.Y, 5);
        Assert.AreEqual(robot.Direction, Direction.NORTH);
    }

    [TestMethod("RobotCommands_LEFT")]
    public void RobotCommands_LEFT()
    {
        //PLACE souht west
        Robot robot = new Robot(_table);
        RobotPlaceCommand command = new RobotPlaceCommand()
        {
            CommandArgs = new string[] { "0", "0", "NORTH" }
        };
        command.Execute(robot);

        //LEFT
        RobotRotateCommand command2 = new RobotRotateCommand(Rotate.LEFT);
        command2.Execute(robot);

        //Validate
        Assert.AreEqual(robot.Direction, Direction.WEST);
    }

    [TestMethod("RobotCommands_RIGHT")]
    public void RobotCommands_RIGHT()
    {
        //PLACE south west
        Robot robot = new Robot(_table);
        RobotPlaceCommand command = new RobotPlaceCommand()
        {
            CommandArgs = new string[] { "0", "0", "NORTH" }
        };
        command.Execute(robot);

        //RIGHT
        RobotRotateCommand command2 = new RobotRotateCommand(Rotate.RIGHT);
        command2.Execute(robot);

        //Validate
        Assert.AreEqual(robot.Direction, Direction.EAST);
    }

    [TestMethod("ConsoleCommands_LargeInput")]
    public void ConsoleCommands_LargeInput()
    {
        //Create and run a large input
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < 10000; i++)
            sb.Append(Path.GetRandomFileName());

        IConsoleCommand? command;
        Assert.IsFalse(_consoleCommandParser.TryParse(sb.ToString(), out command));
    }
}