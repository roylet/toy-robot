# Toy Robot Game

## 1.0 - Run game
### 1.0 - Via terminal

In your terminal run:

```
dotnet run --project .\toy-robot\toy-robot.csproj
```

### 1.2 - Via .exe

1. Run:

    ```
    dotnet publish -c Release -r win10-x64
    ```

2. Go to `toy-robot/bin/Release/net6.0/toy-robot.exe` Double click the .exe file.

### 1.3 - Via Visual Studio

Select `toy-robot` from the solution explorer, then click the play button.

## 2.0 - Run Unit Tests

## 2.1 - Via terminal

In your terminal run:

```
dotnet test
```

## 2.2 - Via Visual Studio

Right click `toy-robot-test` from the solution explorer, then click "Run Tests".