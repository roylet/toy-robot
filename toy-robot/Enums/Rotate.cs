﻿namespace ToyRobot.Enums
{
    /// <summary>
    /// Robot rotate actions
    /// </summary>
    public enum Rotate
    {
        LEFT,
        RIGHT
    }
}
