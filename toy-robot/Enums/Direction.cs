﻿namespace ToyRobot.Enums
{
    /// <summary>
    /// Robot facing directions
    /// </summary>
    public enum Direction
    {
        NORTH = 0,
        EAST = 1,
        SOUTH = 2,
        WEST = 3
    }
}
