﻿using ToyRobot.Enums;
using ToyRobot.Interfaces;
using ToyRobot.Models;
using ToyRobot.Models.Commands;

namespace ToyRobot
{
    /// <summary>
    /// Toy robot game.
    /// </summary>
    public class Game
    {
        private ICommandParser _consoleCommandParser;
        private IRobot _robot;
        private IConsoleCommand[] _robotCommands = new IConsoleCommand[] {
            new CloseCommand()
        };

        public Game(ICommandParser consoleCommandParser, IRobot robot, IRobotCommand[] robotCommands)
        {
            _robotCommands = _robotCommands.Concat(robotCommands).ToArray();
            _consoleCommandParser = consoleCommandParser;
            _consoleCommandParser.ConsoleCommands = _robotCommands;
            _robot = robot;
        }

        /// <summary>
        /// Starts the game.
        /// </summary>
        public void Start()
        {
            //Show welcome message
            GenerateWelcomeMessage();

            while (true)
            {
                Console.Write("Command: ");

                //Collect user input and parse into console command object
                IConsoleCommand? consoleCommand;
                if (_consoleCommandParser.TryParse(_consoleCommandParser.ReadInput(), out consoleCommand) && consoleCommand != null)
                {
                    //Check for the close command
                    if (consoleCommand is CloseCommand)
                        break;

                    //Execute command and return results
                    if (consoleCommand is IRobotCommand)
                    {
                        IRobotCommand robotCommand = (IRobotCommand)consoleCommand;
                        _consoleCommandParser.WriteOutput(robotCommand.Execute(_robot));
                    }
                }

                //Blank line
                Console.WriteLine();
            }
        }

        /// <summary>
        /// Generates a welcome message to the client console
        /// </summary>
        public void GenerateWelcomeMessage()
        {
            Console.WriteLine();
            Console.WriteLine("Toy Robot Game!");
            Console.WriteLine("───────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────");
            foreach (ConsoleCommand command in _consoleCommandParser.ConsoleCommands)
                Console.WriteLine($"{command.ToString()}");
            Console.WriteLine("───────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────");
            Console.WriteLine();
        }
    }
}