
using ToyRobot.Interfaces;
using ToyRobot.Models.Commands;

namespace ToyRobot.Models
{
    public class ConsoleCommandParser : ICommandParser
    {
        /// <inheritdoc/>
        public IConsoleCommand[] ConsoleCommands { get; set; }

        public ConsoleCommandParser(IConsoleCommand[]? consoleCommands = null)
        {
            ConsoleCommands = consoleCommands ?? new ConsoleCommand[] { };
        }

        /// <inheritdoc/>
        public bool TryParse(object? obj, out IConsoleCommand? consoleCommand)
        {
            //Set initial value
            consoleCommand = null;

            //Check object type
            if (obj == null || !this.CheckType(obj))
                return false;

            //Convert to str
            string str = (string)obj;

            //Ensure command is not empty
            if (string.IsNullOrWhiteSpace(str)) return false;

            //Sanitize command and extract arguments
            str = str.Trim().ToUpper();

            //Extract command name and arguments and validate
            string[] data = str.Split(' ', StringSplitOptions.RemoveEmptyEntries | StringSplitOptions.TrimEntries);

            //Argument length is 0
            if (data.Length == 0) return false;

            //Argument given is empty
            if (string.IsNullOrWhiteSpace(data[0])) return false;

            //Set action
            consoleCommand = ConsoleCommands.Where(x => x.CommandAction == data[0]).FirstOrDefault();
            if (consoleCommand == null) return false;

            //Set arguments
            if (data.Length > 1) consoleCommand.CommandArgs = data[1].Split(',').Select(arg => arg.Trim()).ToArray();

            //Valid
            return true;
        }

        /// <inheritdoc/>
        public object? ReadInput()
        {
            return Console.ReadLine();
        }

        /// <inheritdoc/>
        public void WriteOutput(object output)
        {
            Console.WriteLine(output);
        }

        ///<summary>
        /// Checks whether the given type is a string
        ///</summary>
        private bool CheckType(object? obj)
        {
            return obj is string;
        }
    }
}