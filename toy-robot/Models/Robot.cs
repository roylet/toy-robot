﻿using ToyRobot.Interfaces;
using ToyRobot.Enums;

namespace ToyRobot.Models
{
    /// <summary>
    /// Toy robot object
    /// </summary>
    public class Robot : IRobot
    {
        private int _x = 0;
        private int _y = 0;

        /// <inheritdoc/>
        public Direction Direction { get; set; }

        /// <inheritdoc/>
        public ITable Table { get; }

        /// <inheritdoc/>
        public int X
        {
            get { return _x; }
            set { _x = Math.Max(0, Math.Min(value, Table.TableWidth)); }
        }

        /// <inheritdoc/>
        public int Y
        {
            get { return _y; }
            set { _y = Math.Max(0, Math.Min(value, Table.TableHeight)); }
        }

        /// <inheritdoc/>
        public bool IsPlaced { get; set; } = false;

        public Robot(ITable table)
        {
            Table = table;
            Direction = Direction.NORTH;
        }
    }
}