using ToyRobot.Interfaces;
namespace ToyRobot.Models.Commands
{
    public class CloseCommand : ConsoleCommand
    {
        public CloseCommand() : base(
            "CLOSE",
            "CLOSE",
            "Will close the game.")
        { }
    }
}