using ToyRobot.Interfaces;
using ToyRobot.Enums;

namespace ToyRobot.Models.Commands
{
    public class RobotRotateCommand : ConsoleCommand, IRobotCommand
    {
        private Rotate _rotate;

        public RobotRotateCommand(Rotate rotate) : base(
            Enum.GetName(typeof(Rotate), rotate) ?? "",
            Enum.GetName(typeof(Rotate), rotate) ?? "",
            $"Will rotate the robot 90 degrees {Enum.GetName(typeof(Rotate), rotate)} without changing the location of the robot.")
        {
            _rotate = rotate;
        }

        /// <inheritdoc/>
        public string Execute(IRobot robot)
        {
            string? errorMessage = Validate(robot);
            if (errorMessage != null) return errorMessage;

            //Convert enum to int and add/subtract
            int currentRotate = (int)robot.Direction;
            currentRotate = _rotate == Rotate.LEFT ? currentRotate - 1 : currentRotate + 1;

            //Loop direction around if value is outside direction bounds
            int numPositions = Enum.GetNames(typeof(Direction)).Length - 1;
            if (currentRotate < 0) currentRotate = numPositions;
            if (currentRotate > numPositions) currentRotate = 0;

            //Re-apply direction
            robot.Direction = (Direction)currentRotate;

            return "Moved the robot " + CommandAction;
        }

        /// <inheritdoc/>
        public string? Validate(IRobot robot)
        {
            //Ensure robot is placed
            if (!robot.IsPlaced)
                return "Robot must be placed before moving.";

            //Valid
            return null;
        }
    }
}