using ToyRobot.Interfaces;
using ToyRobot.Enums;

namespace ToyRobot.Models.Commands
{
    public class RobotPlaceCommand : ConsoleCommand, IRobotCommand
    {
        public RobotPlaceCommand() : base(
            "PLACE",
            "PLACE X,Y,<Direction>",
            "Will put the toy robot on the table in direction X,Y and facing NORTH, SOUTH, EAST or WEST.")
        { }

        /// <inheritdoc/>
        public string Execute(IRobot robot)
        {
            string? errorMessage = Validate(robot);
            if (errorMessage != null) return errorMessage;
            if (CommandArgs == null) return "";

            //Place robot
            Direction? direction = null;
            if (CommandArgs.Length == 3) direction = (Direction)Enum.Parse(typeof(Direction), CommandArgs[2]);

            //Place robot
            robot.X = int.Parse(CommandArgs[0]);
            robot.Y = int.Parse(CommandArgs[1]);
            robot.Direction = direction ?? robot.Direction;
            robot.IsPlaced = true;

            return $"Robot placed at {robot.X},{robot.Y},{Enum.GetName(typeof(Direction), robot.Direction)}";
        }

        /// <inheritdoc/>
        public string? Validate(IRobot robot)
        {
            //Check whether arguments exist
            //Ensure arguments contain 2 or 3 arguments
            if (CommandArgs == null || CommandArgs.Length < 2 || CommandArgs.Length > 3)
                return "Please format the place command as 'PLACE X,Y,<POSITION>' or 'PLACE X,Y'";

            //Ensure the first two arguments are numbers
            int x, y = 0;
            if (!int.TryParse(CommandArgs[0], out x) || !int.TryParse(CommandArgs[1], out y))
                return "X and Y parameters must me number.";

            //Check whether the place is out of bounds
            if (robot.Table.IsOutOfBounds(x, y))
                return "X and Y coordinates are out of table bounds.";

            //Check whether this is the first time the robot has been placed, if it is, require a direction
            if (!robot.IsPlaced && CommandArgs.Length == 2)
                return "Please specify a facing direction when placing the robot for the first time.";

            //Validate direction argument and run command
            string[] positions = Enum.GetNames(typeof(Direction));
            if (CommandArgs.Length == 3 && !positions.Contains(CommandArgs[2]))
                return "Robot direction must be one of the following: " + String.Join(",", positions);

            //Valid
            return null;
        }
    }
}