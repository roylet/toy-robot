using ToyRobot.Interfaces;
using ToyRobot.Enums;

namespace ToyRobot.Models.Commands
{
    public class RobotMoveCommand : ConsoleCommand, IRobotCommand
    {
        public RobotMoveCommand() : base(
            "MOVE",
            "MOVE",
            "Will move the toy robot one unit forward in the direction it is currently facing.")
        { }

        /// <inheritdoc/>
        public string Execute(IRobot robot)
        {
            string? errorMessage = Validate(robot);
            if (errorMessage != null) return errorMessage;

            //Move robot
            switch (robot.Direction)
            {
                case Direction.NORTH:
                case Direction.SOUTH:
                    robot.Y = robot.Direction == Direction.NORTH ? robot.Y + 1 : robot.Y - 1;
                    break;
                case Direction.WEST:
                case Direction.EAST:
                    robot.X = robot.Direction == Direction.WEST ? robot.X - 1 : robot.X + 1;
                    break;
            }
            return "Moved the robot, run REPORT to view direction";
        }

        /// <inheritdoc/>
        public string? Validate(IRobot robot)
        {
            //Ensure robot is placed
            if (!robot.IsPlaced)
                return "Robot must be placed before moving.";

            //Valid
            return null;
        }
    }
}