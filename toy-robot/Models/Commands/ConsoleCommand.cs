﻿using ToyRobot.Interfaces;

namespace ToyRobot.Models.Commands
{
    public abstract class ConsoleCommand : IConsoleCommand
    {
        /// <inheritdoc/>
        public string CommandAction { get; set; }

        /// <inheritdoc/>
        public string[]? CommandArgs { get; set; }

        /// <inheritdoc/>
        public string CommandActionDescription { get; }

        /// <inheritdoc/>
        public string CommandDescription { get; }

        /// <summary>
        /// Retruns a formatted description
        /// </summary>
        public override string ToString()
        {
            return this.CommandActionDescription.PadRight(30) + this.CommandDescription;
        }

        public ConsoleCommand(string action, string actionDescription, string description)
        {
            CommandAction = action;
            CommandActionDescription = actionDescription;
            CommandDescription = description;
        }

        /// <inheritdoc/>
        public virtual string Execute()
        {
            return "";
        }
    }
}
