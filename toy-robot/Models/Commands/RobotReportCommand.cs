using ToyRobot.Interfaces;
using ToyRobot.Enums;

namespace ToyRobot.Models.Commands
{
    public class RobotReportCommand : ConsoleCommand, IRobotCommand
    {
        public RobotReportCommand() : base(
            "REPORT",
            "REPORT",
            "Will announce the X,Y and orientation of the robot.")
        { }

        /// <inheritdoc/>
        public string Execute(IRobot robot)
        {
            string? errorMessage = Validate(robot);
            if (errorMessage != null) return errorMessage;

            if (robot.IsPlaced) return $"Output: {robot.X},{robot.Y},{Enum.GetName(typeof(Direction), robot.Direction)}";
            return "Robot is not on the table.";
        }

        /// <inheritdoc/>
        public string? Validate(IRobot robot)
        {
            //Valid
            return null;
        }
    }
}