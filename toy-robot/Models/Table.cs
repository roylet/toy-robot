using ToyRobot.Interfaces;
namespace ToyRobot.Models {
    public class Table : ITable {
        private int _tableWidth = 0;
        private int _tableHeight = 0;

        //<inheritdoc/>
        public int TableWidth { get; } = 5;
        //<inheritdoc/>
        public int TableHeight { get; } = 5;

        public Table(int tableWidth, int tableHeight) {
            _tableWidth = tableWidth;
            _tableHeight = tableHeight;
        }

        //<inheritdoc/>
        public bool IsOutOfBounds(int x, int y)
        {
            return x < 0 || x > TableWidth || y < 0 || y > TableHeight;
        }
    }
}