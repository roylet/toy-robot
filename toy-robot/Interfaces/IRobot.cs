using ToyRobot.Enums;

namespace ToyRobot.Interfaces
{
    public interface IRobot
    {
        /// <summary>
        /// Robots current x position
        /// </summary>
        int X { get; set; }

        /// <summary>
        /// Robots current y position
        /// </summary>
        int Y { get; set; }

        /// <summary>
        /// Robots currently assigned table.
        /// </summary>
        ITable Table { get; }

        /// <summary>
        /// Robots current facing direction
        /// </summary>
        Direction Direction { get; set; }

        /// <summary>
        /// Indicates whether the robot has been placed on the table
        /// </summary>
        bool IsPlaced { get; set; }
    }
}