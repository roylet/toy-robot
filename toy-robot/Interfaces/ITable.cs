using ToyRobot.Models;

namespace ToyRobot.Interfaces
{
    public interface ITable
    {
        /// <summary>
        /// Total width of the table
        /// </summary>
        int TableWidth { get; }
        /// <summary>
        //Total height of the table
        /// </summary>
        int TableHeight { get; }
        /// <summary>
        /// Indicates whether the given coordinates are out of bounds based on the table size.
        /// </summary>
        /// <param name="x">x coordinate</param>
        /// <param name="y">y coordinate</param>
        /// <returns>true/false out of bounds.</returns>
        bool IsOutOfBounds(int x, int y);
    }
}