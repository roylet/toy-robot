namespace ToyRobot.Interfaces
{
    public interface IRobotCommand : IConsoleCommand
    {
        /// <summary>
        /// Validates the specific command related to the robot.
        /// </summary>
        /// <param name="robot">Robot context to use for validation</param>
        /// <returns>null if valid and an error message is in error.</returns>
        string? Validate(IRobot robot);

        /// <summary>
        /// Execute a command against the given robot.
        /// </summary>
        /// <param name="robot">Robot context to use to execute the command.</param>
        /// <returns>Output of execution</returns>
        string Execute(IRobot robot);
    }
}