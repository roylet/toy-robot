namespace ToyRobot.Interfaces
{
    public interface IConsoleCommand
    {
        /// <summary>
        /// First word given in the command
        /// </summary>
        public string CommandAction { get; set; }

        /// <summary>
        /// Arguments given after the first action command
        /// </summary>
        public string[]? CommandArgs { get; set; }

        /// <summary>
        /// Description used to communicate the command inputs
        /// </summary>
        public string CommandActionDescription { get; }

        /// <summary>
        /// Description used to communicate the commands intent
        /// </summary>
        public string CommandDescription { get; }

        /// <summary>
        /// Performs an action against the given context.
        /// </summary>
        /// <returns></returns>
        public abstract string Execute();
    }
}