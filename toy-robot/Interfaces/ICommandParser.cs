﻿using ToyRobot.Models.Commands;
namespace ToyRobot.Interfaces
{
    public interface ICommandParser
    {
        /// <summary>
        /// List of valid commands to search when parsing a command.
        /// </summary>
        IConsoleCommand[] ConsoleCommands { get; set; }

        /// <summary>
        /// Takes the users string input and converts it into a valid command. If the command is invalid null is returned.
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        bool TryParse(object? obj, out IConsoleCommand? consoleCommand);

        ///<summary>
        /// Listens for an input value.
        ///</summary>
        object? ReadInput();

        ///<summary>
        /// Writes an output.
        ///</summary>
        void WriteOutput(object output);
    }
}
