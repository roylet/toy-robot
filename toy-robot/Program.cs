﻿using ToyRobot;
using ToyRobot.Enums;
using ToyRobot.Interfaces;
using ToyRobot.Models;
using ToyRobot.Models.Commands;

//Create and start the game
Game game = new Game(new ConsoleCommandParser(), new Robot(new Table(5, 5)), new IRobotCommand[] {
    new RobotPlaceCommand(),
    new RobotMoveCommand(),
    new RobotRotateCommand(Rotate.LEFT),
    new RobotRotateCommand(Rotate.RIGHT),
    new RobotReportCommand()
});
game.Start();